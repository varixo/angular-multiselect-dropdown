# AngularMultiselectDropdown

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.0.

## Source
Partially rewritten original code from [angular2-multiselect-dropdown](https://github.com/CuppaLabs/angular2-multiselect-dropdown#readme). Dropdown is compatible with Angular 9.1.13.
