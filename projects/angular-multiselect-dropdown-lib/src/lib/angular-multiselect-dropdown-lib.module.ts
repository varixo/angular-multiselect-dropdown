import {NgModule} from '@angular/core';
import {AngularMultiSelect} from './angular-multiselect-dropdown-lib.component';
import {ClickOutsideDirective} from './directives/click-outside.directive';
import {ScrollDirective} from './directives/scroll.directive';
import {StyleDirective} from './directives/style.directive';
import {SetPositionDirective} from './directives/set-position.directive';
import {Item} from './components/c-item/c-item.component';
import {Badge} from './components/c-badge/c-badge.component';
import {Search} from './components/c-search/c-search.component';
import {TemplateRenderer} from './components/c-template-renderer/template-renderer.component';
import {CIcon} from './components/c-icon/c-icon.component';
import {ListFilterPipe} from './pipes/list-filter';
import {CommonModule} from '@angular/common';
import {VirtualScrollerModule} from 'ngx-virtual-scroller';
import {FormsModule} from '@angular/forms';
import { AngularMultiselectDropdownLibService } from './angular-multiselect-dropdown-lib.service';


@NgModule({
  declarations: [
    AngularMultiSelect,
    ClickOutsideDirective,
    ScrollDirective,
    StyleDirective,
    SetPositionDirective,
    Item,
    Badge,
    Search,
    TemplateRenderer,
    CIcon,
    ListFilterPipe
  ],
  imports: [
    CommonModule,
    VirtualScrollerModule,
    FormsModule
  ],
  exports: [
    AngularMultiSelect,
    ClickOutsideDirective,
    ScrollDirective,
    StyleDirective,
    SetPositionDirective,
    Item,
    Badge,
    Search,
    TemplateRenderer,
    CIcon,
    ListFilterPipe
  ],
  providers: [
    AngularMultiselectDropdownLibService
  ]
})
export class AngularMultiselectDropdownLibModule { }
