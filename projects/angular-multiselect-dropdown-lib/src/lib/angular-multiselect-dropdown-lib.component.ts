import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostListener, Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output, PLATFORM_ID,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { ListFilterPipe } from './pipes/list-filter';
import { Item } from './components/c-item/c-item.component';
import { Badge } from './components/c-badge/c-badge.component';
import { Search } from './components/c-search/c-search.component';
import { DropdownSettings } from './multiselect.interface';
import { VirtualScrollerComponent } from 'ngx-virtual-scroller';
import { AngularMultiselectDropdownLibService } from './angular-multiselect-dropdown-lib.service';
import { MyException } from './my-exception.model';
import { isPlatformBrowser } from '@angular/common';


export const DROPDOWN_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => AngularMultiSelect),
  multi: true
};
export const DROPDOWN_CONTROL_VALIDATION: any = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => AngularMultiSelect),
  multi: true,
};
const noop = () => {
};

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'angular2-multiselect',
  templateUrl: './angular-multiselect-dropdown-lib.component.html',
  styleUrls: ['./angular-multiselect-dropdown-lib.scss'],
  providers: [DROPDOWN_CONTROL_VALUE_ACCESSOR, DROPDOWN_CONTROL_VALIDATION],
  encapsulation: ViewEncapsulation.None
})
// tslint:disable-next-line:component-class-suffix
export class AngularMultiSelect implements OnInit, ControlValueAccessor, OnChanges, AfterViewInit,
  Validator, AfterViewChecked, OnDestroy {


  constructor(public elementRef: ElementRef,
              private cdr: ChangeDetectorRef,
              private ds: AngularMultiselectDropdownLibService,
              @Inject(PLATFORM_ID) private platformId: any) {
    this.searchTerm$.asObservable().pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      tap(term => term)
    ).subscribe(val => {
      this.filterInfiniteList(val);
    });
  }

  @Input()
  data: Array<any>;

  @Input()
  settings: DropdownSettings;

  @Input()
  loading: boolean;

  // tslint:disable-next-line:no-output-on-prefix
  @Output()
  onSelect: EventEmitter<any> = new EventEmitter<any>();

  // tslint:disable-next-line:no-output-on-prefix
  @Output()
  onDeSelect: EventEmitter<any> = new EventEmitter<any>();

  // tslint:disable-next-line:no-output-on-prefix
  @Output()
  onSelectAll: EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

  // tslint:disable-next-line:no-output-on-prefix
  @Output()
  onDeSelectAll: EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

  // tslint:disable-next-line:no-output-on-prefix
  @Output()
  onOpen: EventEmitter<any> = new EventEmitter<any>();

  // tslint:disable-next-line:no-output-on-prefix
  @Output()
  onClose: EventEmitter<any> = new EventEmitter<any>();

  // tslint:disable-next-line:no-output-on-prefix
  @Output()
  onScrollToEnd: EventEmitter<any> = new EventEmitter<any>();

  // tslint:disable-next-line:no-output-on-prefix
  @Output()
  onFilterSelectAll: EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

  // tslint:disable-next-line:no-output-on-prefix
  @Output()
  onFilterDeSelectAll: EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

  // tslint:disable-next-line:no-output-on-prefix
  @Output()
  onAddFilterNewItem: EventEmitter<any> = new EventEmitter<any>();

  // tslint:disable-next-line:no-output-on-prefix
  @Output()
  onGroupSelect: EventEmitter<any> = new EventEmitter<any>();

  // tslint:disable-next-line:no-output-on-prefix
  @Output()
  onGroupDeSelect: EventEmitter<any> = new EventEmitter<any>();

  @ContentChild(Item, {static: false}) itemTempl: Item;
  @ContentChild(Badge, {static: false}) badgeTempl: Badge;
  @ContentChild(Search, {static: false}) searchTempl: Search;


  @ViewChild('searchInput', {static: false}) searchInput: ElementRef;
  @ViewChild('selectedList', {static: false}) selectedListElem: ElementRef;
  @ViewChild('dropdownList', {static: false}) dropdownListElem: ElementRef;
  @ViewChild('cuppaDropdown', {static: false}) cuppaDropdown: ElementRef;

  virtualdata: any = [];
  searchTerm$ = new Subject<string>();

  filterPipe: ListFilterPipe;
  public selectedItems: Array<any>;
  public isActive = false;
  public isSelectAll = false;
  public isFilterSelectAll = false;
  public isInfiniteFilterSelectAll = false;
  public groupedData: Array<any>;
  filter: any;
  public scrollTop: any;
  public chunkIndex: any[] = [];
  public cachedItems: any[] = [];
  public groupCachedItems: any[] = [];
  public itemHeight: any = 41.6;
  public screenItemsLen: any;
  public totalHeight: any;
  public scroller: any;
  public selectedListHeight: any;
  public filterLength: any = 0;
  public infiniteFilterLength: any = 0;
  public viewPortItems: any;
  public item: any;
  public dropdownListYOffset = 0;
  subscription: Subscription;
  public dropDownWidth = 0;
  public dropDownTop = 0;
  public dropDownLeft = 0;
  public id: any = Math.random().toString(36).substring(2);
  defaultSettings: DropdownSettings = {
    singleSelection: false,
    text: 'Select',
    enableCheckAll: true,
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    filterSelectAllText: 'Select all filtered results',
    filterUnSelectAllText: 'UnSelect all filtered results',
    enableSearchFilter: false,
    searchBy: [],
    maxHeight: 300,
    badgeShowLimit: 999999999999,
    classes: '',
    disabled: false,
    searchPlaceholderText: 'Search',
    showCheckbox: true,
    noDataLabel: 'No Data Available',
    searchAutofocus: true,
    lazyLoading: false,
    labelKey: 'itemName',
    primaryKey: 'id',
    position: 'bottom',
    autoPosition: true,
    enableFilterSelectAll: true,
    selectGroup: false,
    addNewItemOnFilter: false,
    addNewButtonText: 'Add',
    escapeToClose: true,
    clearAll: true,
    tagToBody: true
  };
  randomSize = true;
  public parseError: boolean;
  public filteredList: any = [];
  virtualScroollInit = false;
  @ViewChild(VirtualScrollerComponent, {static: false})
  private virtualScroller: VirtualScrollerComponent;
  public isDisabledItemPresent = false;
  private onTouchedCallback: (_: any) => void = noop;
  private onChangeCallback: (_: any) => void = noop;


  @HostListener('document:keyup.escape', ['$event'])
  onEscapeDown(event: KeyboardEvent): void {
    if (this.settings.escapeToClose) {
      this.closeDropdown();
    }
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event: any): void {
    if (this.isActive) {
      this.closeDropdown();
      const elem = this.cuppaDropdown.nativeElement;
      if (this.settings.autoPosition) {
        this.dropDownTop = elem.getBoundingClientRect().y + elem.clientHeight + 1;
      }
      this.dropDownLeft = elem.getBoundingClientRect().x;
    }
  }

  ngOnInit(): void {
    this.settings = Object.assign(this.defaultSettings, this.settings);

    this.cachedItems = this.cloneArray(this.data);
    if (this.settings.position === 'top') {
      setTimeout(() => {
        this.selectedListHeight = {val: 0};
        this.selectedListHeight.val = this.selectedListElem.nativeElement.clientHeight;
      });
    }
    this.subscription = this.ds.getData().subscribe(data => {
      if (data) {
        let len = 0;
        data.forEach((obj: any) => {
          if (obj.disabled) {
            this.isDisabledItemPresent = true;
          }
          if (!obj.hasOwnProperty('grpTitle')) {
            len++;
          }
        });
        this.filterLength = len;
        this.onFilterChange(data);
      }

    });
    setTimeout(() => {
      this.calculateDropdownDirection();
    });
    this.virtualScroollInit = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.data && !changes.data.firstChange) {
      if (this.settings.groupBy) {
        this.groupedData = this.transformData(this.data, this.settings.groupBy);
        if (this.data.length === 0) {
          this.selectedItems = [];
        }
        this.groupCachedItems = this.cloneArray(this.groupedData);
      }
      this.cachedItems = this.cloneArray(this.data);
    }
    if (changes.settings && !changes.settings.firstChange) {
      this.settings = Object.assign(this.defaultSettings, this.settings);
    }
    if (changes.loading) {
    }
    if (this.settings.lazyLoading && this.virtualScroollInit && changes.data) {
      this.virtualdata = changes.data.currentValue;
    }

    if (this.selectedItems) {
      if (this.selectedItems.length === 0 || this.data.length === 0 || this.selectedItems.length < this.data.length) {
        this.isSelectAll = false;
      }
    }
  }

  ngAfterViewInit(): void {
    if (this.settings.lazyLoading) {
      // this._elementRef.nativeElement.getElementsByClassName("lazyContainer")[0].addEventListener('scroll', this.onScroll.bind(this));
    }
  }

  ngAfterViewChecked(): void {
    if (this.selectedListElem.nativeElement.clientHeight && this.settings.position === 'top' && this.selectedListHeight) {
      this.selectedListHeight.val = this.selectedListElem.nativeElement.clientHeight;
      this.cdr.detectChanges();
    }
    this.calculateDropdownDirection();
  }

  onItemClick(item: any, index: number, evt: Event): boolean {
    if (item.disabled) {
      return false;
    }

    if (this.settings.disabled) {
      return false;
    }

    const found = this.isSelected(item);
    const limit = this.selectedItems.length < this.settings.limitSelection ? true : false;

    if (!found) {
      if (this.settings.limitSelection) {
        if (limit) {
          this.addSelected(item);
          this.onSelect.emit(item);
        }
      } else {
        this.addSelected(item);
        this.onSelect.emit(item);
      }

    } else {
      this.removeSelected(item);
      this.onDeSelect.emit(item);
    }
    if (this.isSelectAll || this.data.length > this.selectedItems.length) {
      this.isSelectAll = false;
    }

    if (this.data.length === this.selectedItems.length) {
      this.isSelectAll = true;
    }
    if (this.settings.groupBy) {
      this.updateGroupInfo(item);
    }


  }

  public validate(c: FormControl): any {
    return null;
  }

  writeValue(value: any): void {
    if (value !== undefined && value !== null && value !== '') {
      if (this.settings.singleSelection) {
        if (this.settings.groupBy) {
          this.groupedData = this.transformData(this.data, this.settings.groupBy);
          this.groupCachedItems = this.cloneArray(this.groupedData);
          this.selectedItems = [value[0]];
        } else {
          try {

            if (value.length > 1) {
              this.selectedItems = [value[0]];
              throw new MyException(404, {msg: 'Single Selection Mode, Selected Items cannot have more than one item.'});
            } else {
              this.selectedItems = value;
            }
          } catch (e) {
            console.error(e.body.msg);
          }
        }

      } else {
        if (this.settings.limitSelection) {
          this.selectedItems = value.slice(0, this.settings.limitSelection);
        } else {
          this.selectedItems = value;
        }
        if (this.selectedItems.length === this.data.length && this.data.length > 0) {
          this.isSelectAll = true;
        }
        if (this.settings.groupBy) {
          this.groupedData = this.transformData(this.data, this.settings.groupBy);
          this.groupCachedItems = this.cloneArray(this.groupedData);
        }
      }
    } else {
      this.selectedItems = [];
    }
  }

  // From ControlValueAccessor interface
  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  // From ControlValueAccessor interface
  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  trackByFn(index: number, item: any): any {
    return item[this.settings.primaryKey];
  }

  isSelected(clickedItem: any): boolean {
    if (clickedItem.disabled) {
      return false;
    }
    let found = false;
    if (this.selectedItems) {
      this.selectedItems.forEach(item => {
        if (clickedItem[this.settings.primaryKey] === item[this.settings.primaryKey]) {
          found = true;
        }
      });
    }
    return found;
  }

  addSelected(item: any): void {
    if (item.disabled) {
      return;
    }
    if (this.settings.singleSelection) {
      this.selectedItems = [];
      this.selectedItems.push(item);
      this.closeDropdown();
    } else {
      this.selectedItems.push(item);
    }
    this.onChangeCallback(this.selectedItems);
    this.onTouchedCallback(this.selectedItems);
  }

  removeSelected(clickedItem: any): void {
    if (this.selectedItems) {
      this.selectedItems.forEach(item => {
        if (clickedItem[this.settings.primaryKey] === item[this.settings.primaryKey]) {
          this.selectedItems.splice(this.selectedItems.indexOf(item), 1);
        }
      });
    }
    this.onChangeCallback(this.selectedItems);
    this.onTouchedCallback(this.selectedItems);
  }

  toggleDropdown(evt: any): boolean {
    if (this.settings.disabled) {
      return false;
    }
    this.isActive = !this.isActive;
    if (this.isActive) {
      if (this.settings.searchAutofocus && this.searchInput && this.settings.enableSearchFilter && !this.searchTempl) {
        setTimeout(() => {
          this.searchInput.nativeElement.focus();
        }, 0);
      }
      this.onOpen.emit(true);
    } else {
      this.onClose.emit(false);
    }
    if (this.settings.lazyLoading) {
      this.virtualdata = this.data;
      this.virtualScroollInit = true;
    }
    evt.preventDefault();
  }

  public openDropdown(): boolean {
    if (this.settings.disabled) {
      return false;
    }
    this.isActive = true;
    if (this.settings.searchAutofocus && this.searchInput && this.settings.enableSearchFilter && !this.searchTempl) {
      setTimeout(() => {
        this.searchInput.nativeElement.focus();
      }, 0);
    }
    this.onOpen.emit(true);
  }

  public closeDropdown(): void {
    if (this.searchInput && this.settings.lazyLoading) {
      this.searchInput.nativeElement.value = '';
    }
    if (this.searchInput) {
      this.searchInput.nativeElement.value = '';
    }
    this.filter = '';
    this.isActive = false;
    this.onClose.emit(false);
  }

  public closeDropdownOnClickOut(): void {
    if (this.isActive) {
      if (this.searchInput && this.settings.lazyLoading) {
        this.searchInput.nativeElement.value = '';
      }
      if (this.searchInput) {
        this.searchInput.nativeElement.value = '';
      }
      this.filter = '';
      this.isActive = false;
      this.clearSearch();
      this.onClose.emit(false);
    }
  }

  toggleSelectAll(event: any): void {
    if (!this.isSelectAll) {
      this.selectedItems = [];
      if (this.settings.groupBy) {
        this.groupedData.forEach((obj) => {
          obj.selected = !obj.disabled;
        });
        this.groupCachedItems.forEach((obj) => {
          obj.selected = !obj.disabled;
        });
      }
      // this.selectedItems = this.data.slice();
      this.selectedItems = this.data.filter((individualData) => !individualData.disabled);
      this.isSelectAll = true;
      this.onChangeCallback(this.selectedItems);
      this.onTouchedCallback(this.selectedItems);

      this.onSelectAll.emit(this.selectedItems);
    } else {
      if (this.settings.groupBy) {
        this.groupedData.forEach((obj) => {
          obj.selected = false;
        });
        this.groupCachedItems.forEach((obj) => {
          obj.selected = false;
        });
      }
      this.selectedItems = [];
      this.isSelectAll = false;
      this.onChangeCallback(this.selectedItems);
      this.onTouchedCallback(this.selectedItems);

      this.onDeSelectAll.emit(this.selectedItems);
    }
    setTimeout(() => {
      this.calculateDropdownDirection();
    });
    event.stopPropagation();
  }

  filterGroupedList(): void {
    if (this.filter === '' || this.filter === null) {
      this.clearSearch();
      return;
    }
    this.groupedData = this.cloneArray(this.groupCachedItems);
    this.groupedData = this.groupedData.filter(obj => {
      let arr = [];
      if (obj[this.settings.labelKey].toLowerCase().indexOf(this.filter.toLowerCase()) > -1) {
        arr = obj.list;
      } else {
        arr = obj.list.filter((t: any) => {
          return t[this.settings.labelKey].toLowerCase().indexOf(this.filter.toLowerCase()) > -1;
        });
      }

      obj.list = arr;
      if (obj[this.settings.labelKey].toLowerCase().indexOf(this.filter.toLowerCase()) > -1) {
        return arr;
      } else {
        return arr.some((cat: any) => {
            return cat[this.settings.labelKey].toLowerCase().indexOf(this.filter.toLowerCase()) > -1;
          }
        );
      }

    });
  }

  toggleFilterSelectAll(): void {
    if (!this.isFilterSelectAll) {
      const added: any[] = [];
      if (this.settings.groupBy) {
        this.ds.getFilteredData().forEach((el: any) => {
          if (!this.isSelected(el) && !el.hasOwnProperty('grpTitle')) {
            this.addSelected(el);
            added.push(el);
          }
        });

      } else {
        this.ds.getFilteredData().forEach((item: any) => {
          if (!this.isSelected(item)) {
            this.addSelected(item);
            added.push(item);
          }

        });
      }

      this.isFilterSelectAll = true;
      this.onFilterSelectAll.emit(added);
    } else {
      const removed: any[] = [];
      if (this.settings.groupBy) {
        this.ds.getFilteredData().forEach((el: any) => {
          if (this.isSelected(el)) {
            this.removeSelected(el);
            removed.push(el);
          }
        });
      } else {
        this.ds.getFilteredData().forEach((item: any) => {
          if (this.isSelected(item)) {
            this.removeSelected(item);
            removed.push(item);
          }

        });
      }
      this.isFilterSelectAll = false;
      this.onFilterDeSelectAll.emit(removed);
    }
  }

  toggleInfiniteFilterSelectAll(): void {
    if (!this.isInfiniteFilterSelectAll) {
      this.virtualdata.forEach((item: any) => {
        if (!this.isSelected(item)) {
          this.addSelected(item);
        }
      });
      this.isInfiniteFilterSelectAll = true;
    } else {
      this.virtualdata.forEach((item: any) => {
        if (this.isSelected(item)) {
          this.removeSelected(item);
        }

      });
      this.isInfiniteFilterSelectAll = false;
    }
  }

  clearSearch(): void {
    if (this.settings.groupBy) {
      this.groupedData = [];
      this.groupedData = this.cloneArray(this.groupCachedItems);
    }
    this.filter = '';
    this.isFilterSelectAll = false;

  }

  onFilterChange(data: any): void {
    if (this.filter && this.filter === '' || data.length === 0) {
      this.isFilterSelectAll = false;
    }
    let cnt = 0;
    data.forEach((item: any) => {

      if (!item.hasOwnProperty('grpTitle') && this.isSelected(item)) {
        cnt++;
      }
    });

    if (cnt > 0 && this.filterLength === cnt) {
      this.isFilterSelectAll = true;
    } else if (cnt > 0 && this.filterLength !== cnt) {
      this.isFilterSelectAll = false;
    }
    this.cdr.detectChanges();
  }

  cloneArray(arr: any): any {
    if (Array.isArray(arr)) {
      return JSON.parse(JSON.stringify(arr));
    } else if (typeof arr === 'object') {
      throw new Error('Cannot clone array containing an object!');
    } else {
      return arr;
    }
  }

  updateGroupInfo(item: any): boolean {
    if (item.disabled) {
      return false;
    }
    const key = this.settings.groupBy;
    if (!!this.groupedData) {
      this.groupedData.forEach((obj: any) => {
        let cnt = 0;
        if (obj.grpTitle && (item[key] === obj[key])) {
          if (obj.list) {
            obj.list.forEach((el: any) => {
              if (this.isSelected(el)) {
                cnt++;
              }
            });
          }
        }
        if (obj.list && (cnt === obj.list.length) && (item[key] === obj[key])) {
          obj.selected = true;
        } else if (obj.list && (cnt !== obj.list.length) && (item[key] === obj[key])) {
          obj.selected = false;
        }
      });
    }
    if (!!this.groupCachedItems) {
      this.groupCachedItems.forEach((obj: any) => {
        let cnt = 0;
        if (obj.grpTitle && (item[key] === obj[key])) {
          if (obj.list) {
            obj.list.forEach((el: any) => {
              if (this.isSelected(el)) {
                cnt++;
              }
            });
          }
        }
        if (obj.list && (cnt === obj.list.length) && (item[key] === obj[key])) {
          obj.selected = true;
        } else if (obj.list && (cnt !== obj.list.length) && (item[key] === obj[key])) {
          obj.selected = false;
        }
      });
    }
  }

  transformData(arr: Array<any>, field: any): Array<any> {
    const groupedObj: any = arr.reduce((prev: any, cur: any) => {
      if (!prev[cur[field]]) {
        prev[cur[field]] = [cur];
      } else {
        prev[cur[field]].push(cur);
      }
      return prev;
    }, {});
    const tempArr: any = [];
    Object.keys(groupedObj).map((x: any) => {
      const obj: any = {};
      const disabledChildrens = [];
      obj.grpTitle = true;
      obj[this.settings.labelKey] = x;
      obj[this.settings.groupBy] = x;
      obj.selected = false;
      obj.list = [];
      let cnt = 0;
      groupedObj[x].forEach((item: any) => {
        item.list = [];
        if (item.disabled) {
          this.isDisabledItemPresent = true;
          disabledChildrens.push(item);
        }
        obj.list.push(item);
        if (this.isSelected(item)) {
          cnt++;
        }
      });
      if (cnt === obj.list.length) {
        obj.selected = true;
      } else {
        obj.selected = false;
      }

      // Check if current group item's all childrens are disabled or not
      obj.disabled = disabledChildrens.length === groupedObj[x].length;
      tempArr.push(obj);
      // obj.list.forEach((item: any) => {
      //     tempArr.push(item);
      // });
    });
    return tempArr;
  }

  public filterInfiniteList(evt: any): void {
    const filteredElems: Array<any> = [];
    if (this.settings.groupBy) {
      this.groupedData = this.groupCachedItems.slice();
    } else {
      this.data = this.cachedItems.slice();
      this.virtualdata = this.cachedItems.slice();
    }

    if ((evt !== null || evt !== '') && !this.settings.groupBy) {
      if (this.settings.searchBy.length > 0) {
        this.settings.searchBy.forEach(item1 => {

          this.virtualdata.filter((el: any) => {
            if (el[item1.toString()].toString().toLowerCase().indexOf(evt.toString().toLowerCase()) >= 0) {
              filteredElems.push(el);
            }
          });
        });

      } else {
        this.virtualdata.filter((el: any) => {
          for (const prop in el) {
            if (el[prop].toString().toLowerCase().indexOf(evt.toString().toLowerCase()) >= 0) {
              filteredElems.push(el);
              break;
            }
          }
        });
      }
      this.virtualdata = [];
      this.virtualdata = filteredElems;
      this.infiniteFilterLength = this.virtualdata.length;
    }
    if (evt.toString() !== '' && this.settings.groupBy) {
      this.groupedData.filter((el: any) => {
        if (el.hasOwnProperty('grpTitle')) {
          filteredElems.push(el);
        } else {
          for (const prop in el) {
            if (el[prop].toString().toLowerCase().indexOf(evt.toString().toLowerCase()) >= 0) {
              filteredElems.push(el);
              break;
            }
          }
        }
      });
      this.groupedData = [];
      this.groupedData = filteredElems;
      this.infiniteFilterLength = this.groupedData.length;
    } else if (evt.toString() === '' && this.cachedItems.length > 0) {
      this.virtualdata = [];
      this.virtualdata = this.cachedItems;
      this.infiniteFilterLength = 0;
    }
    this.virtualScroller.refresh();
  }

  resetInfiniteSearch(): void {
    this.filter = '';
    this.isInfiniteFilterSelectAll = false;
    this.virtualdata = [];
    this.virtualdata = this.cachedItems;
    this.groupedData = this.groupCachedItems;
    this.infiniteFilterLength = 0;
  }

  onScrollEnd(e: any): void {
    if (e.endIndex === this.data.length - 1 || e.startIndex === 0) {

    }
    this.onScrollToEnd.emit(e);

  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

  }

  selectGroup(item: any): boolean {
    if (item.disabled) {
      return false;
    }
    if (item.selected) {
      item.selected = false;
      item.list?.forEach((obj: any) => {
        this.removeSelected(obj);
      });

      this.onGroupDeSelect.emit(item);
      this.updateGroupInfo(item);

    } else {
      item.selected = true;
      item.list?.forEach((obj: any) => {
        if (!this.isSelected(obj)) {
          this.addSelected(obj);
        }

      });
      this.onGroupSelect.emit(item);
      this.updateGroupInfo(item);

    }


  }

  addFilterNewItem(): void {
    this.onAddFilterNewItem.emit(this.filter);
    this.filterPipe = new ListFilterPipe(this.ds);
    this.filterPipe.transform(this.data, this.filter, this.settings.searchBy);
  }

  calculateDropdownDirection(): void {
    if (!isPlatformBrowser(this.platformId)) {
      return;
    }
    const elem = this.cuppaDropdown.nativeElement;
    const dropdownWidth = elem.clientWidth;
    this.dropDownWidth = dropdownWidth;
    this.dropDownLeft = elem.getBoundingClientRect().x;
    if (this.settings.position === 'top' && !this.settings.autoPosition) {
      this.openTowardsTop(true);
    } else if (this.settings.position === 'bottom' && !this.settings.autoPosition) {
      this.openTowardsTop(false);
    }
    if (this.settings.autoPosition) {
      const dropdownHeight = this.dropdownListElem.nativeElement.clientHeight;
      const viewportHeight = document.documentElement.clientHeight;
      const selectedListBounds = this.selectedListElem.nativeElement.getBoundingClientRect();

      const spaceOnTop: number = selectedListBounds.top;
      const spaceOnBottom: number = viewportHeight - selectedListBounds.top;
      if (spaceOnBottom < spaceOnTop && dropdownHeight < spaceOnTop) {
        this.openTowardsTop(true);
      } else {
        this.openTowardsTop(false);
      }
    }

  }

  openTowardsTop(value: boolean): void {
    const elem = this.cuppaDropdown.nativeElement;
    if (value && this.selectedListElem.nativeElement.clientHeight) {
      this.dropdownListYOffset = 15 - this.selectedListElem.nativeElement.clientHeight;
      this.dropDownTop = elem.getBoundingClientRect().y - this.dropdownListElem.nativeElement.clientHeight - 15;
      this.settings.position = 'top';

    } else {
      this.dropDownTop = elem.getBoundingClientRect().y + elem.clientHeight + 1;
      this.dropdownListYOffset = 0;
      this.settings.position = 'bottom';

    }
  }

  clearSelection(e?: any): void {
    if (this.settings.groupBy) {
      this.groupCachedItems.forEach((obj) => {
        obj.selected = false;
      });
    }
    this.clearSearch();
    this.selectedItems = [];
    this.isSelectAll = false;
    this.onChangeCallback(this.selectedItems);
    this.onTouchedCallback(this.selectedItems);
    this.onDeSelectAll.emit(this.selectedItems);
  }

  getValue(target: EventTarget): any {
    return (target as HTMLInputElement).value;
  }
}
