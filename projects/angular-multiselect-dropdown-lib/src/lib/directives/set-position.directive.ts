import {Directive, ElementRef, Input, OnChanges, OnInit} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[setPosition]'
})
export class SetPositionDirective implements OnInit, OnChanges {

 @Input('setPosition') height: number;

  constructor(public el: ElementRef) { }

  ngOnInit() {
    if (this.height) {
      this.el.nativeElement.style.bottom = (this.height + 15) + 'px';
    }
  }

  ngOnChanges(): void {
    if (this.height) {
    this.el.nativeElement.style.bottom = (this.height + 15) + 'px';
  }
}
}
