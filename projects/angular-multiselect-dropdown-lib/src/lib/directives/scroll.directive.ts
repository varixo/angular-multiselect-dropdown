import {Directive, ElementRef, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[scroll]'
})
export class ScrollDirective {
  constructor(private _elementRef: ElementRef) {
  }

  @Output()
  public scroll = new EventEmitter<MouseEvent>();

  @HostListener('scroll', ['$event'])
  public onClick(event: MouseEvent, targetElement: HTMLElement): void {
    this.scroll.emit(event);
  }
}
