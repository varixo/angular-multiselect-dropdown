import {Component, ContentChild, TemplateRef} from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'c-search',
  template: '',
})
// tslint:disable-next-line:component-class-suffix
export class Search {

  @ContentChild(TemplateRef, {static: true}) template: TemplateRef<any>;
  constructor() {
  }

}
