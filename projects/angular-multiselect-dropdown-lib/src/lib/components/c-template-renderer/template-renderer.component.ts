import {Component, EmbeddedViewRef, Input, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'c-templateRenderer',
  template: ''
})
// tslint:disable-next-line:component-class-suffix
export class TemplateRenderer implements OnInit, OnDestroy {

  @Input() data: any;
  @Input() item: any;
  view: EmbeddedViewRef<any>;

  constructor(public viewContainer: ViewContainerRef) {
  }

  ngOnInit() {
    this.view = this.viewContainer.createEmbeddedView(this.data.template, {
      '\$implicit': this.data,
      'item': this.item
    });
  }

  ngOnDestroy() {
    this.view.destroy();
  }


}
